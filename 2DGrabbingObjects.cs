using UnityEngine;
using System.Collections;

//script used for grabbing & throwing 2D objects that are marked with Grabbable tag
//uses the overlap circle method 
public class 2DGrabbingObjects : MonoBehaviour {

	//if something was grabbed
	bool isGrabbed;
	//where the grabbed object will be placed on the player. Needs to be set up in Inspector
	public Transform holdPoint;
	//force applied when throwing and object 
	public float throwForce;
	//will determine the distance at which the object is grabbed
    float grabCheckRadius = 2f;
    //determines what layer to grab. Needs to be set up in Inspector
	public LayerMask grabLayer;
    //determines where to draw the circle to check overlapping objects. Needs to be set up in Inspector
	public Transform grabCheckPosition;
    //variables used to store colliders to grab and throw
	Collider2D grabbable;
	Collider2D grabbed;

	void Update () {
		//check if player is close enough to a grabbable object and store the collider it intersected in a temporary variable
        //temporary variable is updated at every frame
		grabbable = Physics2D.OverlapCircle (grabCheckPosition.position, grabCheckRadius, grabLayer);
		//if player is in vicinity of an object he can grab
        if (grabbable != null) {
			//if player pressed grab button while intersecting a grabbable collider..
			if (Input.GetKeyDown (KeyCode.E) || Input.GetMouseButtonDown (0)) {
				//..then an object was grabbed..
				isGrabbed = true;
				//..so store the grabbed object collider in a variable that will not be overwritten by next frame
				grabbed = grabbable;
			}
		}
		//if player has grabbed an object..
		if (isGrabbed) {
			//..keep the object in the hold point that was determined in the Inspector
			grabbed.transform.position = holdPoint.position;
			//if while holding an object the player clicks a throw button..	
			if (Input.GetKeyDown (KeyCode.R) || Input.GetMouseButtonDown (1)) {
				//..then no objects are currently grabbed
				isGrabbed = false;
				//make sure the object thrown has a rigidbody attached to it and only throw it if it has
				if (grabbed.GetComponent<Rigidbody2D> () != null) {
					//object will be thrown at 45 degrees angle
					grabbed.GetComponent<Rigidbody2D> ().velocity = new Vector2 (transform.localScale.x, 1) * throwForce;
				}
			}
		}
	}
		
}