using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeCount : MonoBehaviour {

	//time allocated to finish the level, can be set in the Inspector
	public float timeLeft;
	//reference to the timer text to update it. Needs to be set in the Inspector
	public Text timerText;
	
    // Use FixedUpdate to calculate time at a constant pace 
	void FixedUpdate () {
        //substract time since last FixedUpdate from the time left
		timeLeft -= Time.deltaTime;
        //do not count time below 0
		if (timeLeft < 0)
			timeLeft = 0;
        //update time text with current time left
		CountTime ();
	}

	void CountTime () {
        //round time to full seconds
		timerText.text = "Time left: " + Mathf.RoundToInt (timeLeft);
	}

}